import click
from .app import app, db
import yaml
from .models import Musique, Artiste, Genre, Possession
from hashlib import sha256

@app.cli.command()
@click.argument('filename')
def loaddb(filename):
    ''' Creates the tables and populates them with data. '''
    db.create_all()
    donnees = yaml.load(open (filename))

    #création de tous les artistes
    artistes = dict()
    for d in donnees:
        a = d["by"]
        if a not in artistes:
            o = Artiste(nom = a)
            db.session.add(o)
            artistes[a] = o
    db.session.commit()

    #création de toutes les musiques
    for d in donnees :
        a = artistes[d["by"]]
        m = Musique(
            titre = d["title"],
            id_Musique = d["entryId"],
            date = d["releaseYear"],
            img = d["img"],
            id_Artiste = a.id_Artiste
        )
        db.session.add(m)
    db.session.commit()

    #creation de tous les genres
    genres = set()
    for d in donnees :
        genre = d["genre"]
        for g in genre:
            genres.add(g)
    db.session.add_all([ Genre(nom=d) for d in genres])
    db.session.commit()

    musiques = Musique.query.all()
    genres = Genre.query.all()
    for d in donnees:
        for musique in musiques:
            if musique.titre == d["title"]:
                for g in d["genre"]:
                    for genre in genres:
                        if genre.nom == g :
                            db.session.add(Possession(id_Musique = musique.id_Musique, id_Genre = genre.id_Genre))
    db.session.commit()



@app.cli.command()
def syncdb():
    db.create_all()

@app.cli.command()
@click.argument('username')
@click.argument('password')
def newuser(username, password):
    m = sha256()
    m.update(password.encode())
    u = User(username = username, password = m.hexdigest())
    db.session.add(u)
    db.session.commit()

@app.cli.command()
@click.argument('username')
@click.argument('password')
def passwd(username, password):
    m = sha256()
    m.update(password.encode())
    u = User.query.get(username)
    if u:
        u.password = m.hexdigest()
        db.session.commit()
    else:
        print("****************************************************")
        print("Il n'y a pas de compte associé au pseudo " + username)
        print("****************************************************")
