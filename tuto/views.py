from .app import app, db
from flask import render_template, url_for, redirect, flash, request
from .models import (get_sample, get_Musiques, get_Artistes, get_Genres, get_Possessions, Musique, User,
Possession, Artiste, Genre, get_Musique, get_Artiste, get_Artiste_Id, get_Musique_Id, get_Genres_liste)
from flask_wtf import FlaskForm
from wtforms import StringField, HiddenField, PasswordField
from wtforms.validators import DataRequired
from hashlib import sha256
from flask_login import login_user, current_user, logout_user, login_required

class AuthorForm(FlaskForm):
    id = HiddenField('id')
    name = StringField('nom', validators = [DataRequired()])

class LoginForm(FlaskForm):
    username = StringField('Pseudo')
    password = PasswordField('Mot de passe')
    next = HiddenField()

    def get_authenticated_user(self):
        user = User.query.get(self.username.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.password else None

@app.route("/")
def home():
    return render_template(
        "home.html",
        title = "DATABASS",
        morceaux = get_sample()
    )

@app.route("/musique/<int:id_Musique>")
def la_Musique(id_Musique):
    musique = get_Musique(id_Musique)
    artist = get_Artiste(musique["id_Artiste"])
    genre = musique["genres"]
    return render_template(
        "musique.html",
        titre = musique["titre"],
        musique = musique,
        artiste = artist,
        genres = genre
    )


@app.route("/artiste/<int:id_Artiste>")
def le_Artiste(id_Artiste):
    a = get_Artiste(id_Artiste)
    return render_template(
        "artiste.html",
        nom = a["nom"],
        lMusic = a["musiques"]
    )


@app.route("/login/", methods = ["GET", "POST"])
def login():
    f = LoginForm()
    if not f.is_submitted():
        f.next.data = request.args.get("next")
    elif f.validate_on_submit():
        user = f.get_authenticated_user()
        if user:
            login_user(user)
            next = f.next.data or (url_for('home'))
            return redirect(next)
    return render_template("login.html", form = f)

@app.route('/logout/')
def logout():
    logout_user()
    return redirect(url_for('home'))
