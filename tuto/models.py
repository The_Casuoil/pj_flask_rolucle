from .app import db, login_manager
from flask_login import UserMixin


class Artiste(db.Model):
    nom = db.Column(db.String(50))
    id_Artiste = db.Column(db.Integer, primary_key=True)

class Genre(db.Model):
    nom = db.Column(db.String(70))
    id_Genre = db.Column(db.Integer, primary_key=True)

class Musique(db.Model):
    titre = db.Column(db.String(50))
    id_Musique = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Integer)
    img = db.Column(db.String(100))
    id_Artiste = db.Column(db.Integer, db.ForeignKey(Artiste.id_Artiste))

    def __repr__(self):
        return "<Musique (%d) %s>" % (self.id_Musique, self.titre)

class Possession(db.Model):
    id_Possess=db.Column(db.Integer, primary_key=True)
    id_Musique=db.Column(db.Integer, db.ForeignKey(Musique.id_Musique))
    id_Genre=db.Column(db.Integer, db.ForeignKey(Genre.id_Genre))

    musique = db.relationship("Musique", backref=db.backref("possession", lazy="dynamic"))
    genre = db.relationship("Genre", backref=db.backref("possession", lazy="dynamic"))

class User(db.Model, UserMixin):
    username = db.Column(db.String(50), primary_key = True)
    password = db.Column(db.String(64))

    def get_id(self):
        return self.username

def get_Musiques():
    return Musique.query.all()

def get_Artistes():
    return Artiste.query.all()

def get_Genres():
    return Genre.query.all()

def get_Possessions():
    return Possession.query.all()

def get_Musique_Id(id_Musique):
    return Musique.query.get(id_Musique)

def get_Artiste_Id(id_Artiste):
    return Artiste.query.get(id_Artiste)

def get_Genre_id(id_Genre):
    return Genre.query.get(id_Genre)

def get_Possession_Id(id_Possess):
    return Possession.query.get(id_Possess)

def get_sample():
    return Musique.query.limit(10).all()

def get_Musique(id_Musique):
    musique = get_Musique_Id(id_Musique)
    genres = get_Genres()
    artistes = get_Artistes()
    res = {}
    res["id_Musique"] = musique.id_Musique
    res["titre"] = musique.titre
    res["date"] = musique.date
    res["img"] = musique.img
    res["id_Artiste"] = musique.id_Artiste
    res["genres"] = set()
    for a in artistes:
        if(a.id_Artiste == musique.id_Artiste):
            res["artiste"]= a.nom
    possessions = get_Possessions()
    for possess in possessions:
        if(musique.id_Musique==possess.id_Musique):
            for g in genres:
                if(possess.id_Genre==g.id_Genre):
                    res["genres"].add(g.nom)
    return res

def get_Artiste(id_Artiste):
    musiques = get_Musiques()
    artiste = get_Artiste_Id(id_Artiste)
    res = {}
    res["id_Artiste"]=id_Artiste
    res["nom"]=artiste.nom
    res["musiques"] = set()
    for mus in musiques:
        if(artiste.id_Artiste==mus.id_Artiste):
            res["musiques"].add(mus.titre)
    return res

def get_Genres_liste(id_Musique):
    res = []
    possessions = get_Possessions()
    for p in possessions:
        if p.id_Musique == id_Musique:
            res.append(p.id_Genre)
    return res;

@login_manager.user_loader
def load_user(username):
    return User.query.get(username)
