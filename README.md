Prérequis : avoir installé :
-Python3
-Venv
-Pip

Installation :
  -cloner le dépôt :
$ git clone https://gitlab.com/The_Casuoil/pj_flask_rolucle.git

  -sans changer de répertoire, créer un environement Venv :
$ virtualenv -p python3 venv

  -démarrer l'environnement Venv :
$ source venv/bin/activate

  -aller dans le répertoire du projet :
$ cd pj_flask_rolucle

  -installer les modules python :
$ pip install -r requirement.txt

  -créer la base de données :
$ flask loaddb tuto/data.yml

  -lancer l'app :
$ flask run

  -aller sur votre nivigateur et entrer l'URL suivante :
$ http://localhost:5000/
